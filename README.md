# RabbitListener

**Listener script for RabbitMQ message broker.**


Required python packages: pika, pyyaml

To install latest version: pip3 install --no-cache-dir git+https://git.vedur.is/eysteinn/rabbitlistener.git

Example command line to listen to inc and sifted topics:

```bash
listen_topic.py --uri amqp://<RABBITMQ_USER>:<RABBITMQ_PASS>@<RABBITMQ_HOST>:5672/sat-stream inc sifted.*
```

  

### Example 1: listen_example.py
Code implementation for basic listener can be seen in example/listen_example.py:


The example connect to RabbitMQ and creates a temp queue and listens to topic 'inc' and prints the message to screen.
Remember to replace 'username' and 'password'
User can include their own message handling in the callback function.

```python
from rabbitlistener.consumer import Consumer
consumer = Consumer()
consumer.connect('testvm118.vedur.is', 'username', 'password')
queue_name = consumer.create_queue(routing_keys='inc')
def callback(cons, channel, basic_deliver, properties, body):
    print("Topic: {topic}\n{body}".format(topic=basic_deliver.routing_key, body=body.decode("utf-8")))
consumer.consume_queue(queue_name=queue_name, on_message_callback=callback)
```


### Example 2: listen_threaded_example.py
This is a threaded version of a listener, similar to previous example except the process callback function is on another thread.  The function process_finished is called on the main thread when the worker thread is finished and can grap return values from the worker thread.  

The Consumer class is not thread-safe so be careful when using it on another thread.

This multithread solution is useful when processing takes long time since rabbitmq requires heartbeats or it disconnects the client. So if processing is done on a seperate thread the heartbeats are still sent on the main thread avoiding disconnects.

Here auto_ack is turned of so we need to manually acknowledge messages when processing is finished, otherwise a new processing thread is started immediately for each message (since they are acked right away), which can overwhelm the system if there are many messages in the queue.

```python
import time
import threading
from rabbitlistener.consumer import Consumer

consumer = Consumer()
consumer.connect('testvm118.vedur.is', 'username', 'password')

queue_name = consumer.create_queue(routing_keys='inc')

def process(basic_deliver, properties, body):
    print('Processing message with topic: {0} on new thread: {1}'.format(basic_deliver.routing_key, threading.get_native_id()))
    time.sleep(3)
    return {'text':'sometext', 'value': 10, 'success': True}

def process_finished(basic_deliver, process_return, **kwargs):
    print('Back to main thread with thread_id: {0}, subthread returned: {1}'.format(threading.get_native_id(), process_return))
    consumer.acknowledge_message(basic_deliver.delivery_tag)

print('Main thread with thread_id: {thread_id}'.format(thread_id=threading.get_native_id()))
consumer.threaded_consume_queue(queue_name=queue_name, threaded_process=process, on_finished=process_finished, auto_ack=False)
```

