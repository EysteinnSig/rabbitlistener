#!/usr/bin/env python3

import time
import threading
from rabbitlistener.consumer import Consumer

def main(uri, topics, queue, durable, **kwargs):
    consumer = Consumer()
    consumer.connect_uri(uri)

    queue_name = consumer.create_queue(queue_name=queue, routing_keys=topics, durable=durable)

    def process(basic_deliver, properties, body):
        # Do processing here on seperate thread...
        print('Processing message with topic: {0} on new thread: {1}'.format(basic_deliver.routing_key, threading.get_native_id()))
        time.sleep(3)
        return {'text':'sometext', 'value': 10, 'success': True}

    def process_finished(basic_deliver, process_return, **kwargs):
        # Back to main thread after processing is finished.
        print('Back to main thread with thread_id: {0}, subthread returned: {1}'.format(threading.get_native_id(), process_return))
        consumer.acknowledge_message(basic_deliver.delivery_tag)

    print('Main thread with thread_id: {thread_id}'.format(thread_id=threading.get_native_id()))
    consumer.threaded_consume_queue(queue_name=queue_name, threaded_process=process, on_finished=process_finished, auto_ack=False)

def parse_arguments():
    import argparse
    from rabbitlistener.rabbit_argparser import parse_rabbitmq_connection_arguments, parse_rabbitmq_queue_arguments, parse_rabbitmq_topic_arguments
    parser = argparse.ArgumentParser()
    parse_rabbitmq_queue_arguments(parser)
    parse_rabbitmq_topic_arguments(parser)
    parse_rabbitmq_connection_arguments(parser)
    parser.add_argument('--durable', action='store_true', help='.')

    args = vars(parser.parse_args())
    return args

if __name__ == '__main__':
    args=parse_arguments()
    main(**args)
