import time
import threading
from rabbitlistener.consumer import Consumer

consumer = Consumer()
consumer.connect('testvm118.vedur.is', 'username', 'password')

queue_name = consumer.create_queue(routing_keys='inc')

def process(basic_deliver, properties, body):
    print('Processing message with topic: {0} on new thread: {1}'.format(basic_deliver.routing_key, threading.get_native_id()))
    time.sleep(3)
    return {'text':'sometext', 'value': 10, 'success': True}

def process_finished(basic_deliver, process_return, **kwargs):
    print('Back to main thread with thread_id: {0}, subthread returned: {1}'.format(threading.get_native_id(), process_return))
    consumer.acknowledge_message(basic_deliver.delivery_tag)

print('Main thread with thread_id: {thread_id}'.format(thread_id=threading.get_native_id()))
consumer.threaded_consume_queue(queue_name=queue_name, threaded_process=process, on_finished=process_finished, auto_ack=False)
