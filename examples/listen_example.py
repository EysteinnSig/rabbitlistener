from rabbitlistener.consumer import Consumer

consumer = Consumer()
consumer.connect('testvm118.vedur.is', 'username', 'password')

queue_name = consumer.create_queue(routing_keys='inc')

def callback(cons, channel, basic_deliver, properties, body):
    print("Topic: {topic}\n{body}".format(topic=basic_deliver.routing_key, body=body.decode("utf-8")))

consumer.consume_queue(queue_name=queue_name, on_message_callback=callback)