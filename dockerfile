# Barebones Apache installation on Ubuntu

FROM ubuntu

MAINTAINER Veðurstofan / Icelandic Meteorological Office

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    git \
    inotify-tools 

RUN pip3 install pika

ARG UNAME=sat
ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME
#https://stackoverflow.com/questions/44683119/dockerfile-replicate-the-host-user-uid-and-gid-to-the-image
WORKDIR /home/sat

#COPY requirements.txt /tmp/
#RUN pip install -r /tmp/requirements.txt

#RUN apt-get install -y vim


COPY bin /home/sat/bin
COPY rabbitlistener/emitter.py /home/sat/rabbitlistener/
RUN chown -R sat:sat /home/sat/

#RUN rabbitmqctl add_user sat sat
#RUN rabbitmqctl set_user_tags sat administrator
#RUN rabbitmqctl set_permissions -p / sat ".*" ".*" ".*"

CMD ["/home/sat/bin/rabbit_watch", "/data/", "amqp://sat:sat@vtm01:5672/%2F"]
#CMD ["/bin/bash"]
#CMD ["/home/sat/bin/endless"]
