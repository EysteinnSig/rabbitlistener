from setuptools import setup

setup(name='rabbitlistener',
      version='0.1',
      description='Listener to RabbitMQ message broker',
      url='https://git.vedur.is/eysteinn/rabbitlistener',
      author='Eysteinn',
      author_email='eysteinn@vedur.is',
      license='MIT',
      packages=['rabbitlistener'],
      install_requires=['pika','pyyaml'],
      zip_safe=False,
      scripts=['rabbitlistener/satmessage.py', 'rabbitlistener/emitter.py', 'rabbitlistener/listen_topic.py', 'rabbitlistener/sat2proc.py']
)
