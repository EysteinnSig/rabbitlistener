import logging

import pika


class Consumer(object):
    """Consumer consumes messages.
    """

    def __init__(
            self,
            exchange='sat-stream', #'topics_logs',
            exchange_type='topic'):  # , amqp_url):
        self._connection = None
        self._channel = None
        self._parameters = None
        self._queue_name = None
        self._on_message_callback = None
        self._exchange = exchange
        self._exchange_type = exchange_type

    def _connect(self, connection_parameters, prefetch_count):
        connection = pika.BlockingConnection(connection_parameters)
        channel = connection.channel()
        channel.basic_qos(prefetch_count=prefetch_count, global_qos=True)

        channel.exchange_declare(
            exchange=self._exchange,
            exchange_type=self._exchange_type,
            passive=True)

        self._connection = connection
        self._channel = channel
        self._connection_parameters = connection_parameters

    def connect_uri(self, amqp_url, prefetch_count=1):
        parameters = pika.URLParameters(amqp_url)
        self._connect(parameters, prefetch_count=prefetch_count)

    def connect(
            self,
            hostname='localhost',
            user='sat',
            passw='sat',
            port=5672,
            vhost='sat-stream',
            prefetch_count=1):
        credentials = pika.PlainCredentials(user, passw)
        parameters = pika.ConnectionParameters(
            hostname, port, vhost, credentials)
        self._connect(parameters, prefetch_count=prefetch_count)

    def create_queue(
            self,
            queue_name="",
            routing_keys=[],
            TTL=None,
            durable=False,
            auto_delete=True):
        assert(self._connection)
        assert(self._channel)

        if isinstance(routing_keys, str):
            routing_keys = routing_keys.split(',')
            #routing_keys = [routing_keys]

        channel = self._channel
        arguments = {}
        arguments["x-queue-type"] = "classic"
        if TTL:
            arguments['x-message-ttl'] = TTL

        result = channel.queue_declare(
            queue=queue_name,
            durable=durable,
            auto_delete=auto_delete,
            arguments=arguments)
        if not queue_name:
            queue_name = result.method.queue
        logging.info(
            "Created queue: {0},  auto_delete: {1}".format(
                queue_name, auto_delete))

        for routing_key in routing_keys:
            print('binding to exchange: {exchange}, queue: {queue}, topics: {routing_key}'.format(
                exchange=self._exchange, queue=queue_name, routing_key=routing_key))
            channel.queue_bind(exchange=self._exchange,
                               queue=queue_name,
                               routing_key=routing_key)
        self._queue_name = queue_name
        return queue_name

    def on_message(self, channel, basic_deliver, properties, body):
        if self._on_message_callback:
            self._on_message_callback(
                self, channel, basic_deliver, properties, body)

    def emit(self, topics, message):
        assert(self._channel)
        if not isinstance(topics, list):
            topics = [topics]
        for topic in topics:
            self._channel.basic_publish(exchange=self._exchange,
                                        routing_key=topic,
                                        body=message)

    def consume_queue(self, queue_name="", on_message_callback=None, auto_ack=True):
        if not queue_name:
            queue_name = self._queue_name
        channel = self._channel
        self._on_message_callback = on_message_callback
        if not isinstance(queue_name, (list, set)):
            queue_name = [queue_name]

        for queue in queue_name:
            channel.basic_consume(queue, self.on_message, auto_ack=auto_ack)

        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
        self._connection.close()

    def threaded_consume_queue(consumer, queue_name, threaded_process, on_finished, auto_ack=False):
        import threading
        import functools

        def do_work(consumer, channel, basic_deliver, properties, body):
            ret = None
            try:
                ret = threaded_process(basic_deliver=basic_deliver, properties=properties, body=body)
            finally:
                cb = functools.partial(on_finished, consumer=consumer, process_return=ret, basic_deliver=basic_deliver)

                consumer._connection.add_callback_threadsafe(cb)

        def on_message(consumer, channel, basic_deliver, properties, body):
            thread = threading.Thread(target=do_work, args=(consumer, channel, basic_deliver, properties, body))
            thread.start()

        consumer.consume_queue(queue_name=queue_name, on_message_callback=on_message, auto_ack=auto_ack)

    def get_queue(self, queue_name="", on_message_callback=None, auto_ack=True, max_message_count=-1):
        if not queue_name:
            queue_name = self._queue_name
        channel = self._channel
        self._on_message_callback = on_message_callback

        message_count = 0
        while message_count != max_message_count:
            method_frame, header_frame, body = channel.basic_get(queue=queue_name, auto_ack=auto_ack)
            if method_frame and on_message_callback:
                message_count = message_count + 1
                self.on_message(channel=channel, basic_deliver=method_frame, properties=header_frame, body=body)
            else:
                break
            if message_count == max_message_count:
                break

        return message_count

    def reject_message(self, delivery_tag):
        assert(self._channel)
        self._channel.basic_nack(delivery_tag)

    def acknowledge_message(self, delivery_tag):
        assert(self._channel)
        """Acknowledge the message delivery from RabbitMQ by sending a
    Basic.Ack RPC method for the delivery tag.

    :param int delivery_tag: The delivery tag from the Basic.Deliver frame

    """
        self._channel.basic_ack(delivery_tag)


def parse_arguments():
    """Parser command line arguments into dictionary."""
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--exchange", default='sat-stream', help="RabbitMQ exchange to connect to.")
    parser.add_argument('--uri', default='', help='Advanced Message Queuing Protocol (AMQP) URI:  amqp://<RABBITMQ_USER>:<RABBITMQ_PASS>@<RABBITMQ_HOST>:5672/sat-stream')
    parser.add_argument('--auto-ack', action='store_true', help='Auto ack the message.')
    parser.add_argument('--ack', action='store_true', help='Should message be acked.')
    parser.add_argument('--queue', default='', help='Queue to process.')
    parser.add_argument('--topics', default='', help='Topics to listen to.')
    parser.add_argument('--out-dir', default='', help='Save message to yaml file under give directory. Filename is "<key>_<timestamp>.yaml"')
    parser.add_argument('--maxcount', default=-1, type=int, help='Stop after this many messages.')
    parser.add_argument('--quiet', action='store_true', help='Dont print to screen.')
    parser.add_argument('--auto-delete', action='store_true', help='Should queue be deleted after last consumer disconnect. Only applies if queue was just created.')
    opts = parser.parse_args()
    params = vars(opts)
    if not params['uri']:
        print('Trying to build uri from environmental variables.')
        import os
        params['uri'] = 'amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@{RABBITMQ_HOST}:5672/sat-stream'.format(**os.environ)
    return params


def main():
    params = parse_arguments()
    print(params)
    params['msgcount'] = 0
    consumer = Consumer(exchange=params['exchange'])
    consumer.connect_uri(params['uri'])
    if params['topics']:
        consumer.create_queue(queue_name=params['queue'], routing_keys=params['topics'].split(','), auto_delete=params['auto_delete'], durable=True)

    def callback(cons, channel, basic_deliver, properties, body):
        params['msgcount'] += 1
        if isinstance(body, bytes):
            body = body.decode('utf-8')

        if not params['quiet']:
            print(
                '[{routing}]: {body}'.format(
                    routing=basic_deliver.routing_key,
                    body=body))

        if params['out_dir']:
            from datetime import datetime
            from pathlib import Path
            import random
            import string

            key = basic_deliver.routing_key.split('.')[-1]
            dt = datetime.utcnow()
            rnd = ''.join(random.choices(string.ascii_letters + string.digits, k=6))
            outpath = Path(params['out_dir']) / '{key}_{datetime:%Y%m%dT%H%M%S.%f}_{rnd}.yaml'.format(key=key, datetime=dt, rnd=rnd)
            outpath.write_text(body)

        if params['ack']:
            consumer.acknowledge_message(basic_deliver.delivery_tag)
        else:
            consumer.reject_message(basic_deliver.delivery_tag)

        if params['msgcount'] == params['maxcount']:
            cons._connection.close()
            exit(0)

    consumer.consume_queue(queue_name=params['queue'], on_message_callback=callback, auto_ack=params['auto_ack'])


if __name__ == '__main__':
    main()
