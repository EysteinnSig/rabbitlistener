#!/usr/bin/env python3

import pika


class Emitter(object):
    def __init__(self, exchange='sat-stream'):
        self._connection = None
        self._channel = None
        self._exchange = exchange
        
        

    def _connect(self, connection_parameters):
        connection = pika.BlockingConnection(connection_parameters)
        channel = connection.channel()
        self._connection = connection
        self._channel = channel

    def connect_uri(self, amqp_url):
        parameters = pika.URLParameters(amqp_url) #'amqp://sat:sat@localhost:5672/%2F')
        self._connect(parameters)


    def send(self, topics, message):
        assert(self._channel)
        #print('Emitting: {0}', message)
        self._channel.basic_publish(exchange=self._exchange,
                      routing_key=topics,
                      body=message)

def parse_arguments():
    """Parser command line arguments into dictionary."""
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--exchange", default='sat-stream', help="Hostname of rabbitmq.")
    parser.add_argument("message", default=None, nargs='?')
   
    parser.add_argument('--uri', default='amqp://sat:sat@localhost:5672/%2F', help='Advanced Message Queuing Protocol (AMQP) URI.')
    parser.add_argument('--topic', default=None, help="Topic to send message to.")
    #parser.add_argument('--queue', default=None, help="Queue name.")
    opts = parser.parse_args()
    params = vars(opts)
    return params



def main():
    import sys

    params = parse_arguments()
    print(params)
    #if not params['message'] or not sys.stdin.isatty():
    #    params['message'] = sys.stdin.read()

    #print(params)
    emitter = Emitter()
    emitter.connect_uri(params['uri'])
    #result = emitter._channel.queue_declare(params['queue'], passive=True)
    
    emitter.send(params['topic'], params['message'])
    #consumer = Consumer()
    #consumer.connect_uri(params['uri'])
    #consumer.create_queue(routing_keys=params['topics'])



if __name__ == '__main__':
    main()

