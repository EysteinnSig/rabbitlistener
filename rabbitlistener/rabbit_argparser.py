import argparse
import os



def parse_rabbitmq_topic_arguments(parser, required=False):
    def get_default_topics():
        topics = os.environ.get('RABBITMQ_TOPIC') or ""
        return topics.split(',')
    #def get_topic_from_env():
    #    topics = os.environ.get('RABBITMQ_TOPIC') or ""
    #    return topics.split(',')
    #parser.add_argument("--topics", default=get_topic_from_env(), required=required, help="RabbitMQ topics to connect listen for.")
    parser.add_argument("--topics", default=get_default_topics(), required=required, help="RabbitMQ topics to connect listen for.")


def parse_rabbitmq_queue_arguments(parser, required=False):
    def get_queue_from_env():
        return os.environ.get('RABBITMQ_QUEUE') or ""
    parser.add_argument("--queue", default=get_queue_from_env(), required=required, help="RabbitMQ queue to connect to.")
        

def get_uri_from_env():
    if os.environ.get('RABBITMQ_USER') and os.environ.get('RABBITMQ_PASS') and os.environ.get('RABBITMQ_HOST'):
        print('Building uri from environmental variables.')
        return 'amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@{RABBITMQ_HOST}:5672/sat-stream'.format(**os.environ)
    return None
    
def parse_rabbitmq_connection_arguments(parser):
    """Parser command line arguments into a workable URI."""

    parser.add_argument("--exchange", default='sat-stream', help="RabbitMQ exchange to connect to. Defaults to 'sat-stream'.")
    parser.add_argument('--uri', default=get_uri_from_env(), help='Advanced Message Queuing Protocol (AMQP) URI:  amqp://<RABBITMQ_USER>:<RABBITMQ_PASS>@<RABBITMQ_HOST>:5672/sat-stream')
    #parser.add_argument('topics', metavar='topics', type=str, nargs='+', help='Topics to listen to.')


def test_parse_arguments():
    parser = argparse.ArgumentParser()
    parse_rabbitmq_connection_arguments(parser)
    opts = parser.parse_args()
    params = vars(opts)
    #if not params['uri']:
    #    print('Trying to build uri from environmental variables.')
    #    import os
    #    params['uri'] = 'amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@{RABBITMQ_HOST}:5672/sat-stream'.format(**os.environ)
    return params


if __name__ == "__main__":
    print(test_parse_arguments())