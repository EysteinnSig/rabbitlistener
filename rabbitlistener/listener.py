#!/usr/bin/env python3

from datetime import datetime
import pika


class RabbitListener(object):
    """Simple class to listen to rabbitmq channels."""

    def __init__(self):
        self.channel = None
        self.queue_name = None
        self.connection = None

    """Connect to rabbitmq server."""
    def connect(self, hostname='localhost', user='sat', passw='sat', binding_keys=['inc'], name=''):
        exchange = 'sat-stream'
        credentials = pika.PlainCredentials(user, passw)
        connection = pika.BlockingConnection(pika.ConnectionParameters(hostname, 5672, '/', credentials))

        channel = connection.channel()
        channel.exchange_declare(exchange=exchange, exchange_type='topic')
        channel.basic_qos(prefetch_count=1)

        result = channel.queue_declare(name)
        queue_name = result.method.queue

        for binding_key in binding_keys:
            channel.queue_bind(exchange=exchange,
                               queue=queue_name,
                               routing_key=binding_key)

        self.channel = channel
        self.queue_name = queue_name
        self.connection = connection

        return self


    def consume(self, callback):
        pass 

    """Synchronously listen and return on first message."""
    def listen_sync(self, to_yaml=True):

        channel = self.channel
        queue_name = self.queue_name
        if (channel is None or queue_name is None):
            raise Exception('Not connected')

        method_frame = None
        while True:
            method_frame, header_frame, body = channel.basic_get(queue_name, auto_ack=False)
            if method_frame:
                key = method_frame.routing_key.split('.')[-1]
                if to_yaml:
                    import yaml
                    message = yaml.load(body, Loader=yaml.FullLoader)
                else:
                    message = body
                return message, key

    """Closes connection."""
    def close(self):
        if self.channel:
            self.channel.stop_consuming()
        if self.connection:
            self.connection.close()

        self.channel = None
        self.queue_name = None
        self.connection = None

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()


def parse_arguments():
    """Parser command line arguments into dictionary."""
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--hostname", default='localhost', help="Hostname of rabbitmq.")
    parser.add_argument("--user", default='sat', help="Username for rabbitmq.")
    parser.add_argument("--pass", default='sat', help="Password for rabbitmq.")
    parser.add_argument("--return", action='store_true', help="Returns immediately after first message.")
    parser.add_argument("--tofile", help="Save messages to files.")
    parser.add_argument('channels', metavar='channels', type=str, nargs='+', help='Channels to listen to.')
    opts = parser.parse_args()
    params = vars(opts)
    return params


if __name__ == "__main__":
    params = parse_arguments()

    try:
        with RabbitListener().connect(hostname=params.get('hostname'), binding_keys=params.get('channels'), user=params.get('user'), passw=params.get('pass')) as conn:
            while True:
                message, key = conn.listen_sync(to_yaml=False)
                if message:
                    if isinstance(message, bytes):
                        message = message.decode('utf-8')
                    print(message)

                    if params.get('tofile'):
                        filename = '{key}_{datetime:%Y%m%dT%H%M%S.%f}.yaml'.format(key=key, datetime=datetime.utcnow())
                        with open(filename, 'w') as stream:
                            stream.write(message)

                    if params.get('return'):
                        break
    except KeyboardInterrupt:
        pass
