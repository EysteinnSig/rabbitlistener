#!/usr/bin/env python3


from rabbitlistener.consumer import Consumer
import yaml
import os
import pdb
from pathlib import Path
#import threading
#import functools
from rabbitlistener.satmessage import SatMessage

#from satpy.utils import debug_on
#debug_on()  
import logging
logging.getLogger("pika").setLevel(logging.WARNING)
import string
#from satpy.scene import Scene
#from satpy.resample import get_area_def
import subprocess

script_dir = Path(__file__).resolve().parent



def load_config(config_file = (script_dir / Path('../etc/sat2proc.yaml')).resolve()):
    """Loads config file 'etc/sat2proc.yaml' that is located in etc folder."""
    import yaml
    #import pdb; pdb.set_trace()
    #config_file = (script_dir / Path('../etc/sat2prioc.yaml')).resolve()
    config = {}
    if config_file.is_file():
      config = yaml.full_load(config_file.read_text())
      print('Loaded config file: {filename}'.format(filename=config_file))
    else:
        print('No config file found at {filename}'.format(filename=config_file))

    return config
    #    self.config = yaml.load(stream, Loader=yaml.FullLoader)


def parse_arguments():
    import argparse
    from rabbitlistener.rabbit_argparser import parse_rabbitmq_connection_arguments, parse_rabbitmq_queue_arguments, parse_rabbitmq_topic_arguments

    parser = argparse.ArgumentParser()
    #script_dir = Path(__file__).resolve().parent
    #parser.add_argument("--exchange", default='sat-stream', help="Hostname of rabbitmq.")
    #parser.add_argument('--queue', default='process_sat-process', help='Name of queue to consume.')
    parse_rabbitmq_queue_arguments(parser)
    parse_rabbitmq_topic_arguments(parser)
    #parser.add_argument('--uri', default='amqp://sat:sat@vtm01:5672/%2F', help='Advanced Message Queuing Protocol (AMQP) URI: amqp://<user>:<passw>@<host>:5672/<vhost>')
    parse_rabbitmq_connection_arguments(parser)
    parser.add_argument('--durable', action='store_true', help='.')

    parser.add_argument('--auto-delete', action='store_true', help='Delete queue on disconnect.')
    #parser.add_argument('--log-dir', default=(script_dir / '../logs/').resolve(), help='Directory of log files.')
    parser.add_argument('--log-dir', default=None, help='Directory of log files.')

    parser.add_argument('--config', default=(script_dir / '../etc/sat2proc.yaml').resolve(), help='Config file for commands.')
    parser.add_argument('--message-dir', default=(script_dir / '../queue/').resolve(), help='Directory to store  message files.')
    parser.add_argument('commands', metavar='commands', type=str, nargs='*', help='Commands to execute. Allowed macros: {root_dir}, {key}, {message_path}')
   
    opts = parser.parse_args()
    params = vars(opts)
    return params


def main():
    params=parse_arguments()
    #params['uri'] = 'amqp://sat:sat@localhost:5672/%2F'
    #params['uri'] = 'amqp://sat:sat@eumetcast-hvs3:5672/%2F'
    #params['uri'] = 'amqp://sat:sat@vtm01:5672/%2F'
    
    print(params)
    
    queue_name = params['queue']
    consumer = Consumer(exchange=params['exchange'])
    consumer.connect_uri(params['uri'], prefetch_count=1)

    if params.get('topics'):
        if not params.get('queue'):
            
            queue_name = consumer.create_queue(routing_keys=params['topics'], auto_delete=params['auto_delete'], durable=params['durable'])
            print('Creating queue: {queue_name}'.format(queue_name=queue_name))
        else:
            consumer.create_queue(queue_name=queue_name, routing_keys=params['topics'], auto_delete=params['auto_delete'], durable=params['durable'])

    
#    consumer.create_queue(queue_name = "hrit", routing_keys=params['topics'], TTL=1000*30, auto_delete=False)
#consumer.create_queue(params['queue'], routing_keys=params['topics'], TTL=1000*30, auto_delete=False)
    def threaded_process(basic_deliver, body, **kwargs):
        
        from pathlib import Path
        #print(kwargs)
        from rabbitlistener.satmessage import SatMessage
        config = load_config(params['config'])
        message = SatMessage()
        key = basic_deliver.routing_key.split('.')[-1]
        message.read_text(body)

        #messagefile = message.generate_filename(key, script_dir / '../queue/').resolve() # '/home/sat/logs/')
        messagefile = message.generate_filename(key, params['message_dir']).resolve()
        
        messagefile.write_text(str(message), encoding='utf-8')
        
        print('Saving to {filename}'.format(filename=messagefile))
        commands = list(params['commands'])
        if not commands:            
            print('No commands provided.')
            
            key_config = config.get('keys',{}).get(key, None)
            if key_config and 'commands' in key_config:
                commands = key_config['commands']
                print('Using commands from config: ', commands)
            else:
                print('No commands in config file.')
        
        if not commands:
            return 1
        
        meta = {}
        meta['files'] = ' '.join([str(f) for f in message.get_filenames()])
        meta.update(message.compile_meta()) #CompileMeta())
        meta['key']=key
        meta['message_path'] = messagefile
        meta['root_dir'] = script_dir.parent

        for index, value in enumerate(commands):
            commands[index] = commands[index].format(**meta)
            #commands[index] = commands[index].format(root_dir=script_dir.parent, key=key, message_path=messagefile, **meta)

        key_config = config.get('keys',{}).get(key, None)
        logfile = None
        if key_config and 'logfile' in key_config:
            logfile = key_config['logfile']
            logfile = Path(logfile.format(root_dir=script_dir.parent, key=key, message_path=messagefile))
            logfile.touch()

        
        

        #import pdb; pdb.set_trace()
        #logfile = Path(params['log_dir']) / messagefile.with_suffix('.yaml.log').name
        #logfile.touch()
        #commands=['ls']
        #import pdb; pdb.set_trace()
        if logfile:
            #with logfile.open() as f:
            with open(str(logfile),'w') as f:
                #f=open(str(logfile),'w')
                print('Logging to file: {logfile}'.format(logfile=logfile))
                res = subprocess.run(commands, stdout=f, stderr=subprocess.STDOUT) #, bufsize=1, universal_newlines=True)
        else:
            res = subprocess.run(commands)
        #txt = "_success.yaml.log" if res.returncode==0 else "_failure.yaml.log"
        #newname=messagefile.parent / Path(str(messagefile.stem)+txt)
        #logfile = messagefile.parent / Path(str(messagefile.stem)+txt)
        #messagefile.rename(newname)
        #import pdb; pdb.set_trace()

        return {'returncode': res.returncode, 'messagefile': str(messagefile)}
        

    def on_finished(basic_deliver, process_return, **kwargs):
        from datetime import datetime
        #print(kwargs)
        print('Finished!  Process_return: {process_return}'.format(process_return=process_return))
        dtnow = datetime.utcnow()
        #logfile=script_dir.parent / 'logs' / 'sat2proc_{datetime:%Y%m%d}.log'.format(datetime=dtnow)
        if params['log_dir'] and params['log_dir'].is_dir():
            logfile = Path(params['log_dir']) / 'sat2proc_{datetime:%Y%m%d}.log'.format(datetime=dtnow)

            result_txt = "Success" if process_return['returncode']==0 else "Failure"
        #logfile.write_text('{datetime:%Y%m%dT%H%M%S.f}'.format(datetime=dtnow)))
        #line = '[{datetime:%Y/%m/%d %H:%M:%S.%f}] {messagefile.name} => {result}'.format(datetime=dtnow, message_file=Path(process_return['messagefile']), result=txt )
            line = '[{datetime:%Y/%m/%d %H:%M:%S.%f}] {messagefile.name} => {result}\n'.format(datetime=dtnow, messagefile=Path(process_return['messagefile']), result=result_txt)
        #if logfile.parent.is_dir():
            with logfile.open('a') as f:
                f.write(line)

        consumer.acknowledge_message(basic_deliver.delivery_tag)

    consumer.threaded_consume_queue(queue_name=queue_name.split(','), auto_ack=False, threaded_process=threaded_process, on_finished=on_finished)
    #consumer.consume_queue(queue_name=queue_name, on_message_callback=on_message, auto_ack=False)
    consumer.close()

if __name__ == "__main__":
    main()
