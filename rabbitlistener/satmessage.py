#!/usr/bin/env python3

import os
import yaml

try:
  basestring
except NameError:
  basestring = str

class GenericMessage(object):

    def find_files(self, datadir='/data/sat/inc'):
        from pathlib import Path
        
        if isinstance(datadir, (str, bytes)):
            datadir = Path(datadir)

        allfiles = set([datadir / Path(f['inc']['file']) for f in self.item['files']])
        exists = set()
        missing = set()
        for f in allfiles:
            if f.is_file():
                exists.add(f)
            else:
                missing.add(f)
        return {'all': allfiles, 'missing': missing, 'found': exists, 'is_complete': not missing}


    def download_ftp(self, destination='/data/sat/inc/'):
        #from ftplib import FTP
        import ftplib

        files_by_hosts = {}
        for infile in self.item['files']:
            host = infile['inc']['hostname']
            src_path = infile['inc']['path']
            dst_path = os.path.join(destination, os.path.basename(src_path))
            if not os.path.exists(dst_path):
                
                v=files_by_hosts.get(host,[])
                v = v+[(src_path, dst_path)]
                files_by_hosts[host] = v
        #import pdb; pdb.set_trace()
        for host in files_by_hosts:
            #remove files that already exists from files_by_hosts[host]
            print('Connectiong to host: {host}'.format(host=host))
            with ftplib.FTP(host) as ftp:
                ftp.login()
                #ftp.set_pasv(False)
                for src_path, dst_path in files_by_hosts[host]:
                    #filename = os.path.basename(path)
                    with open(dst_path, 'wb') as f:
                    #from pathlib import Path
                    #with Path(dst_path) as f:
    #                    import pdb; pdb.set_trace()
                        print('Copying {src_path} => {dst_path}'.format(src_path=src_path, dst_path=dst_path))
                        try:
                            retstr=ftp.retrbinary('RETR ' + src_path, f.write)
                            print(retstr)
                        except ftplib.error_perm as msg:
                            #import pdb; pdb.set_trace()

                            #print('File not found on server: ', msg)
                            print(msg)
                            os.remove(dst_path)
                            #import pdb; pdb.set_trace()
                            return False
                        #except Exception as e:
                        #    print(e)   
        return True

    
class SatMessage(GenericMessage):

    def __init__(self, item=None):
        if isinstance(item, dict):
            self.item = item
        elif item:
            self.read(item)

    def __str__(self):
        return yaml.dump(self.item)

    def generate_filename(self, key, dirname = None, result=None):
        from pathlib import Path
        from datetime import datetime
        
        if not dirname:
            dirname = Path()
        if isinstance(dirname, (str, bytes)):
            dirname = Path(dirname)
        dt = datetime.utcnow()
        result_str = '_success' if result is True else '_failure' if result is False else ''

        pathname = dirname / '{key}_{dt:%Y%m%dT%H%M%S.%f}{result}.yaml'.format(key=key, dt=dt, result=result_str)
        return pathname


    def extract_start_end_times(self):
        start_time = [file['sift']['start_time'] for file in self.item['files'] if 'sift' in file and 'start_time' in file['sift']] or None
        end_time = [file['sift']['end_time'] for file in self.item['files'] if 'sift' in file and 'end_time' in file['sift']] or None
        if start_time:
            start_time = min(start_time)
        if end_time:
            end_time = max(end_time)

        return start_time, end_time
    

    def combine_sift(self):
        sifts = [file['sift'] for file in self.item['files']]
        sift_common = set(sifts[0].items())
        for sift in sifts:
            sift_common = sift_common.intersection(set(sift.items()))

        sift_dict = dict(sift_common)
        start_time, end_time = self.extract_start_end_times()
        if start_time: sift_dict['start_time'] = start_time
        if end_time: sift_dict['end_time'] = end_time
        return sift_dict
        #return dict(sift_common)
    
    def compile_meta(self):
        ret = self.combine_sift()
        #ret.update(self.item['meta'])
        ret.update(self.item.get('meta',{}))
        return ret


    def read_yaml(self, yamlstr):
        self.item = yaml.full_load(yamlstr)
        #if isinstance(self.item, basestring): # list of files translates to string with yaml.load
        #    self.ReadToken(yamlstr)
        #else:
        if isinstance(self.item, dict) and 'directory' in self.item and 'file' in self.item and 'hostname' in self.item:
            self.item = {'files': [ {'inc': self.item } ]}

        if isinstance(self.item, list):
            self.item = {'files': self.item}
        if 'inc' in self.item:
            self.item = {'files': [self.item]}
        

    def read_text(self, text):
        try:
            self.read_yaml(text)
        except:
            self.ReadToken(text)


        
    def read_file(self, filename):
        #
        filename = filename.strip()

        with open(filename, 'r') as f:
            data = f.read()
        if filename.lower().endswith('.yaml'):
            self.read_yaml(data)
        else:
            self.ReadToken(data)
            #try:
            #    print('reading f')
            #    self.ReadYaml(f)
            #except:
            #    print('reading: {0}'.format(f))
            #    self.ReadFilelist(f)


            #self.item = yaml.load(f)


    def read(self, yamlorfile):
        try:
            self.read_file(yamlorfile)
        except:
            self.read_yaml(yamlorfile)
            #self.item = yamlorfile

    def get_filenames(self, basedir=None):
        from pathlib import Path
        if basedir:
            if isinstance(basedir, (str, bytes)):
                basedir=Path(basedir)
            return [basedir / Path(f['inc']['file']) for f in self.item['files']]
        return [Path(f['inc']['path']) for f in self.item['files']]
        #return self.GetFilenames()


def test():
    input_test_filename='/home/eysteinn/projects/testdata/queue/ears-viirs_20181026T093024.775801.yaml'
    i=SatMessage(input_test_filename)
    i.CompileMeta()
    input_str=input_test_filename



def parse_arguments():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--print", default=None, help="Print formatted text")
    parser.add_argument("--input", default=None, help="Input yaml work item.")
    parser.add_argument("--meta", default={}, help="Additional meta information.")
    parser.add_argument("--ftpget", default=None, help="Tries to download the files to destination") 
    parser.add_argument('--stdin', action='store_true', help='Pipe yaml message from stdin.')
    #parser.add_argument('filenames', metavar='filenames', type=str, nargs='*', help='Input files for reader.')
    opts = parser.parse_args()
    params = vars(opts)
    return params


def main2():
    import sys
    if not sys.stdin.isatty():
        #print("Reading files from stdin")
        intxt = sys.stdin.read()
        #print(intxt)
    #print('Input: >{0}<'.format(intxt))
    formatstr = sys.argv[1]

    ii = SatMessage()
    ii.ReadText(intxt)
    #print(ii.item)
    host_path_file = ''.join(['{0}\t{1}\t{2}\n'.format(file['inc']['hostname'], file['inc']['directory'], file['inc']['file'])  for file in ii.item['files']])
    filenames = ' '.join(ii.GetFilenames())
    meta = ii.CompileMeta()
    info = dict(ii.item)
    info.update(ii.CompileMeta())
    print(formatstr.format(yaml.dump(ii.item), host_path_file=host_path_file, filenames=filenames, yaml_meta=yaml.dump(meta, default_flow_style=True), **info))


def main():
    import sys
    intxt=""
    params = parse_arguments()
    if params.get('stdin') and not sys.stdin.isatty():
        intxt = sys.stdin.read()
    inputitems = SatMessage()
    if intxt:
      inputitems.ReadText(intxt)
    else:
      inputitems.read_file(params['input'])

    if 'ftpget' in params and params['ftpget']:
      ret = inputitems.download_ftp(params['ftpget'])
      if not ret:
          return 1

    if 'print' in params and params['print']:
      filenames = ' '.join([str(f) for f in inputitems.get_filenames()])
      #meta = inputitems.CompileMeta()
      print(params['print'].format(filenames=filenames))

if __name__ == "__main__":
    exit(main())
