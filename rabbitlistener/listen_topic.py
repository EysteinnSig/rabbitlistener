#!/usr/bin/env python3

from rabbitlistener.consumer import Consumer


def parse_arguments():
    """Parser command line arguments into dictionary."""
    import argparse
    from rabbitlistener.rabbit_argparser import parse_rabbitmq_queue_arguments, parse_rabbitmq_connection_arguments

    parser = argparse.ArgumentParser()
    parse_rabbitmq_connection_arguments(parser)
    parse_rabbitmq_queue_arguments(parser)
    parser.add_argument('topics', metavar='topics', type=str, nargs='+', help='Topics to listen to.')
    parser.add_argument('--keep', dest='auto_delete', action='store_false', help='Keep queue on disconnect.')

    opts = parser.parse_args()
    params = vars(opts)
    return params


if __name__ == '__main__':
    params = parse_arguments()
    consumer = Consumer(exchange=params['exchange'])
    consumer.connect_uri(params['uri'])
    topics = params['topics']
    
    queue_name = consumer.create_queue(routing_keys=topics, queue_name=params['queue'], auto_delete=params['auto_delete'], durable=True)
    
    def callback(cons, channel, basic_deliver, properties, body):
        print("Topic: {topic}\n{body}".format(topic=basic_deliver.routing_key, body=body.decode("utf-8")))

    consumer.consume_queue(queue_name=queue_name, on_message_callback=callback, auto_ack=True)
